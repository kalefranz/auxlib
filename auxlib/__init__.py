# -*- coding: utf-8 -*-
"""auxiliary library to the python standard library"""
from auxlib.packaging import get_version
__version__ = get_version('auxlib')
__author__ = 'Kale Franz'
__contact__ = 'kale@franz.io'
__homepage__ = 'https://github.com/kalefranz/auxlib'
__license__ = "ISC"
__copyright__ = "(c) 2015 Kale Franz. All rights reserved."
